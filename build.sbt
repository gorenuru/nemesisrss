name := "NemesisRss"

version := "1.0"

scalaVersion := "2.10.2"

libraryDependencies ++= List(
  "com.typesafe.slick" %% "slick" % "1.0.1",
  "ch.qos.logback" % "logback-classic" % "1.0.13",
  "com.h2database" % "h2" % "1.3.173",
  "com.typesafe.akka" %% "akka-actor" % "2.2.0-RC1",
  "rome" % "rome" % "1.0",
  "com.typesafe" % "config" % "1.0.2",
  "c3p0" % "c3p0" % "0.9.1.2",
  "org.clapper" %% "grizzled-slf4j" % "1.0.1",
  "com.github.tototoshi" %% "slick-joda-mapper" % "0.4.0"
)