package org.nemesis.rss.logic

import akka.actor.Actor
import grizzled.slf4j.Logging
import org.nemesis.rss.dto.ParsedItem
import org.nemesis.rss._
import scala.slick.lifted.Query
import org.nemesis.rss.dao.{FeedItems, ReadItems, UserFeeds}
import scala.slick.driver.H2Driver.simple._
import scala.slick.session.Database
import Database.threadLocalSession
import org.nemesis.rss.config.DatabaseConfiguration._
import org.nemesis.rss.domain.{ReadItem, FeedItem}

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 12:51 PM
 *
 * Responsible for feed item's retrieval and merging capabilities
 */
class FeedItemsManager extends Actor with Logging {
  def receive = {
    case StoreItems(feed, items) => mergeAndStore(feed, items)
    case GetUnreadItems(userId) => retrieveItems(userId: Long)
    case MarkAsRead(user, items) => markAsRead(user, items)
  }

  /**
   * Handle incoming feeds by merging them with what we already have in the database
   * @param feed - feed id
   * @param items - incomming feed items
   */
  def mergeAndStore(feed: Long, items: List[ParsedItem]) {
    val itemsMap = items.map(item => item.url.hash -> item).toMap

    database.withSession {
      debug(s"Received: ${items.length} items for feedId: $feed. Merging.")
      val hashed = itemsMap.keys.toList
      val persisted = Query(FeedItems).filter(_.hashedUrl inSet hashed).map(_.hashedUrl).list()
      val diff = hashed.diff(persisted)
      debug(s"${persisted.length} of them are persisted. ${diff.length} need to be stored.")

      itemsMap.filter { case (key, _) => diff.contains(key) }.foreach { case (hash, item) =>
        FeedItems.forInsert insert (item.url, hash, item.title, item.content, item.date, feed)
      }
    }
  }

  /**
   * Retrieve items for specified user
   * @param userId - user id
   */
  def retrieveItems(userId: Long) {
    database.withSession {
      val items = for {
        ((userFeed, feedItem), readItem) <- UserFeeds leftJoin FeedItems on (_.feedId === _.feedId) leftJoin ReadItems on((fi, ri) => fi._2.id === ri.itemId)
        if userFeed.userId === userId && readItem.id.isNull
      } yield feedItem
      sender ! ItemsList(items.sortBy(_.date.desc).list())
    }
  }

  /**
   * Mark specified records as "read" by saving records into database
   * @param userId - user id
   * @param items item id list
   */
  def markAsRead(userId: Long, items: List[Long]) {
    database.withSession{
      items.foreach {item =>
        debug(s"Marking item: $item")
        ReadItems.forInsert insert (userId, item)
      }
    }
  }
}

case class StoreItems(feedId: Long, items: List[ParsedItem])
case class GetUnreadItems(userId: Long)
case class ItemsList(items: List[FeedItem])

case class MarkAsRead(userId: Long, items: List[Long])