package org.nemesis.rss.logic

import akka.actor.Actor
import org.nemesis.rss.dao.Users
import scala.slick.session.Database
import grizzled.slf4j.Logging
import org.nemesis.rss.config.DatabaseConfiguration
import DatabaseConfiguration._
import org.nemesis.rss._
import org.nemesis.rss.domain.User
import scala.slick.lifted.Query
import scala.slick.driver.H2Driver.simple._
import Database.threadLocalSession

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 9:03 AM
 */
class UserManager extends Actor with Logging {
  def receive = {
    case RegisterUser(name, password) => registerUser(name, password)
    case Authenticate(name, password) => authenticate(name, password)
  }

  private def registerUser(name: String, password: String) {
    val encryptedPass = password.hash
    database.withSession {
      val response = if (Query(Users.filter(_.name === name).exists).first()) {
        debug(s"User $name already exists.")
        UserAlreadyExists(name)
      } else {
        val id = Users.forInsert insert (name, encryptedPass)
        debug(s"Registered user [name = $name, password = $encryptedPass] with id = $id")
        UserRegistered(id)
      }
      sender ! response
    }
  }

  private def authenticate(name: String, password: String) {
    debug(s"Authenticating user $name")
    val user = Query(Users).filter(user => user.name === name && user.password === password.hash).firstOption
    sender ! AuthenticationResult(user)
  }
}

case class RegisterUser(name: String, password: String)
case class UserRegistered(id: Long)
case class UserAlreadyExists(name: String)

case class Authenticate(name: String, password: String)
case class AuthenticationResult(user: Option[User])