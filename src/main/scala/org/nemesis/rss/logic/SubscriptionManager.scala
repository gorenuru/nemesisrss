package org.nemesis.rss.logic

import scala.slick.driver.H2Driver.simple._
import akka.actor.Actor
import org.nemesis.rss.dao.{UserFeeds, Feeds, Users}
import scala.slick.lifted.Query
import org.nemesis.rss.config.DatabaseConfiguration._
import scala.slick.session.Database
import Database.threadLocalSession
import grizzled.slf4j.Logging

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 10:46 AM
 */
class SubscriptionManager extends Actor with Logging {
  def receive = {
    case Subscribe(user, feed) => subscribe(user, feed)
  }

  private def subscribe(user: Long, feed: String) {
    database.withSession {
      val feedId = Query(Feeds).filter(_.url === feed).map(_.id).firstOption.getOrElse(Feeds.forInsert insert feed)

      val result = user match {
        case usr if !Query(Users.filter(_.id === user).exists).first() => {
          debug(s"User $usr not found")
          UserNotFound(usr)
        }
        case usr if Query(UserFeeds.filter(feed => feed.userId === user && feed.feedId === feedId).exists).first() => {
          debug(s"User $usr already subscribed to $feed")
          AlreadySubscribed(user, feed)
        }
        case _ => {
          debug(s"Subscribing user $user to $feed")
          val userFeedId = UserFeeds.forInsert insert (user, feedId)
          Subscribed(user, userFeedId, feedId, feed)
        }
      }
      sender ! result
    }
  }
}

case class Subscribe(userId: Long, feedUrl: String)
case class UserNotFound(userId: Long)
case class AlreadySubscribed(userId: Long, feedUrl: String)
case class Subscribed(userId: Long, subscriptionId: Long, feedId: Long, feedUrl: String)