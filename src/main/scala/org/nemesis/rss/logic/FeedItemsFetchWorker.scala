package org.nemesis.rss.logic

import akka.actor.Actor
import com.sun.syndication.io.{XmlReader, SyndFeedInput}
import java.net.URL
import scala.collection.JavaConversions._
import com.sun.syndication.feed.synd.SyndEntry
import org.nemesis.rss.dto.ParsedItem
import org.nemesis.rss.config.ActorConfiguration._
import grizzled.slf4j.Logging
import com.sun.syndication.feed.module.DCModule
import org.joda.time.{LocalDateTime, DateTime}

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 3:59 PM
 *
 * Actual feed fetcher
 */
class FeedItemsFetchWorker extends Actor with Logging {
  def receive = {
    case CollectFeedItems(feedId, url) => fetchFeed(feedId, url)
  }

  /**
   * Fetch specified feed
   * @param feedId - feed id
   * @param url - feed url
   */
  private def fetchFeed(feedId: Long, url: String) = {
    debug(s"Fetching feed[url = $url, id = $feedId]")
    val feed = new SyndFeedInput().build(new XmlReader(new URL(url)))
    val items = feed.getEntries
      .map(_.asInstanceOf[SyndEntry])
      .map { entry =>
        val date = entry
          .getModules
          .headOption
          .map(_.asInstanceOf[DCModule])
          .map(module => new LocalDateTime(module.getDate))

        ParsedItem(
          url = entry.getLink,
          title = entry.getTitle,
          date = date.getOrElse(new LocalDateTime()),
          content = Option(entry.getDescription).map(_.getValue).getOrElse("")
        )
      }.toList
    debug(s"Fetched ${items.length} items.")
    feedItemsManager ! StoreItems(feedId, items)
  }
}

case class CollectFeedItems(id: Long, url: String)