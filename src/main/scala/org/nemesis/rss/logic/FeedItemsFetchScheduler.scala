package org.nemesis.rss.logic

import akka.actor.{Props, Actor}
import akka.routing.RoundRobinRouter
import org.nemesis.rss.config.Configuration._
import scala.slick.lifted.Query
import org.nemesis.rss.dao.Feeds
import scala.slick.session.Database
import Database.threadLocalSession
import scala.slick.driver.H2Driver.simple._
import org.nemesis.rss.config.DatabaseConfiguration._

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 3:57 PM
 *
 * Responsible for scheduling fetch job between workers
 */
class FeedItemsFetchScheduler extends Actor {
  val workers = context.actorOf(
    Props[FeedItemsFetchWorker].withRouter(
      RoundRobinRouter(nrOfInstances = properties.getInt("rss.job.numberOfWorkers"))
    )
  )

  def receive = {
    case FetchNow() => scheduleJob()
  }

  /**
   * Retrieve all feeds and schedule fetch job on all the workers
   */
  private def scheduleJob() {
    database.withSession {
      Query(Feeds).foreach { feed => workers ! CollectFeedItems(feed.id.get, feed.url) }
    }
  }
}

case class FetchNow()
