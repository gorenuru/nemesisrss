package org.nemesis

import org.nemesis.rss.util.ExtendedString

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 10:14 AM
 */
package object rss {
  implicit def stringToExtendedString(string: String): ExtendedString = new ExtendedString(string)
}
