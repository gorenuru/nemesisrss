package org.nemesis.rss.config

import com.typesafe.config.ConfigFactory


/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 8:50 AM
 *
 * Contains loaded properties file
 */
object Configuration {
  val properties = ConfigFactory.load()
}
