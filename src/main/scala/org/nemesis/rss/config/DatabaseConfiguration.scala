package org.nemesis.rss.config

import com.mchange.v2.c3p0.ComboPooledDataSource
import scala.slick.session.Database
import Configuration._
import Database.threadLocalSession
import org.nemesis.rss.dao._
import scala.slick.driver.H2Driver.simple._
import grizzled.slf4j.Logging

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 9:42 AM
 *
 * Contains database - related configuration
 */
object DatabaseConfiguration extends Logging {
  val database = {
    val ds = new ComboPooledDataSource
    ds.setDriverClass(properties.getString("rss.database.driver"))
    ds.setJdbcUrl(properties.getString("rss.database.url"))
    Database.forDataSource(ds)
  }

  database.withSession {
    debug("Creating tables")
    (Users.ddl ++ Feeds.ddl ++ UserFeeds.ddl ++ FeedItems.ddl ++ ReadItems.ddl).create
  }
}
