package org.nemesis.rss.config

import akka.actor.{Props, ActorSystem}
import akka.util.Timeout
import scala.concurrent.duration._
import org.nemesis.rss.logic._
import Configuration._

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 10:07 AM
 *
 * Contains actor-related configuration and actor links for DI
 */
object ActorConfiguration {
  val actorSystem = ActorSystem("rss-akka")
  implicit val timeout = Timeout(20 seconds)

  val userManager = actorSystem.actorOf(Props[UserManager])
  val subscriptionManager = actorSystem.actorOf(Props[SubscriptionManager])
  val feedItemsManager = actorSystem.actorOf(Props[FeedItemsManager])
  val feedItemFetchScheduler = actorSystem.actorOf(Props[FeedItemsFetchScheduler])

  val fetchInterval = properties.getInt("rss.job.pollEvery").seconds
  val timer = {
    implicit val context = actorSystem.dispatcher
    actorSystem.scheduler.schedule(fetchInterval, fetchInterval, feedItemFetchScheduler, FetchNow())
  }
}
