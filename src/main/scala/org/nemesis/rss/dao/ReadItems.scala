package org.nemesis.rss.dao

import scala.slick.driver.H2Driver.simple._
import org.nemesis.rss.domain.ReadItem
import scala.slick.lifted.ColumnBase

/**
 * User: aalbul
 * Date: 9/13/13
 * Time: 2:51 PM
 */
object ReadItems extends Table[ReadItem]("READ_ITEMS") {
  def id = column[Long]("ID", O.AutoInc, O.PrimaryKey)
  def itemId = column[Long]("ITEM_ID", O.NotNull)
  def userId = column[Long]("USER_ID", O.NotNull)

  foreignKey("READ_ITEMS_ITEM_FK", itemId, FeedItems)(_.id)
  foreignKey("READ_ITEMS_USER_FK", userId, Users)(_.id)
  index("READ_ITEMS_ITEM_SER_IDX", itemId ~ userId, unique = true)

  def * : ColumnBase[ReadItem] = id.? ~ itemId ~ userId <> (ReadItem, ReadItem.unapply _)
  def forInsert = itemId ~ userId returning ReadItems.id
}
