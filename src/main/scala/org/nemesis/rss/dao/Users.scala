package org.nemesis.rss.dao

import scala.slick.driver.H2Driver.simple._
import org.nemesis.rss.domain.User
import scala.slick.lifted.ColumnBase

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 8:16 AM
 */
object Users extends Table[User]("USERS") {
  def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)
  def name = column[String]("NAME", O.NotNull)
  def password = column[String]("PASSWORD", O.NotNull)

  def idx = index("NAME_IDX", name, unique = true)

  def * : ColumnBase[User] = id.? ~ name ~ password <> (User, User.unapply _)
  def forInsert = name ~ password returning Users.id
}
