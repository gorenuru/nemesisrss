package org.nemesis.rss.dao

import scala.slick.driver.H2Driver.simple._
import org.nemesis.rss.domain.FeedItem
import scala.slick.lifted.ColumnBase
import com.github.tototoshi.slick.JodaSupport._
import org.joda.time.LocalDateTime

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 12:40 PM
 */
object FeedItems extends Table[FeedItem]("FEED_ITEMS") {
  def id = column[Long]("ID", O.AutoInc, O.PrimaryKey)
  def url = column[String]("URL", O.NotNull)
  def hashedUrl = column[String]("HASHED_URL", O.NotNull)
  def title = column[String]("TITLE", O.NotNull)
  def content = column[String]("CONTENT", O.NotNull)
  def date = column[LocalDateTime]("DATE", O.NotNull)
  def feedId = column[Long]("FEED_ID", O.NotNull)

  index("URL_IDX", url, unique = true)
  index("HASHED_URL_IDX", hashedUrl, unique = true)

  def feed = foreignKey("FEED_ITEM_FEED_FK", feedId, Feeds)(_.id)

  def * : ColumnBase[FeedItem] = id.? ~ url ~ hashedUrl ~ title ~ content ~ date ~ feedId <> (FeedItem, FeedItem.unapply _)
  def forInsert = url ~ hashedUrl ~ title ~ content ~ date ~ feedId returning FeedItems.id
}
