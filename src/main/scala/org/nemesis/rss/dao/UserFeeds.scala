package org.nemesis.rss.dao

import scala.slick.driver.H2Driver.simple._
import org.nemesis.rss.domain.UserFeed
import scala.slick.lifted.ColumnBase

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 11:50 AM
 */
object UserFeeds extends Table[UserFeed]("USER_FEEDS") {
  def id = column[Long]("ID", O.AutoInc, O.PrimaryKey)
  def userId = column[Long]("USER_ID", O.NotNull)
  def feedId = column[Long]("FEED_ID", O.NotNull)

  def user = foreignKey("USER_FEED_USER_FK", userId, Users)(_.id)
  def feed = foreignKey("USER_FEED_FEED_FK", feedId, Feeds)(_.id)

  def * : ColumnBase[UserFeed] = id.? ~ userId ~ feedId <> (UserFeed, UserFeed.unapply _)
  def forInsert = userId ~ feedId returning UserFeeds.id
}
