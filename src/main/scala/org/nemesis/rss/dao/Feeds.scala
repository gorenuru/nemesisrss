package org.nemesis.rss.dao

import scala.slick.driver.H2Driver.simple._
import org.nemesis.rss.domain.Feed
import scala.slick.lifted.ColumnBase

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 10:49 AM
 */
object Feeds extends Table[Feed]("FEEDS") {
  def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)
  def url = column[String]("URL", O.NotNull)

  index("FEED_IDX", url, unique = true)

  def * : ColumnBase[Feed] = id.? ~ url <> (Feed, Feed.unapply _)
  def forInsert = url returning Feeds.id
}
