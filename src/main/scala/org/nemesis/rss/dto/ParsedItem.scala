package org.nemesis.rss.dto

import org.joda.time.LocalDateTime

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 1:05 PM
 */
case class ParsedItem(url: String, title: String, content: String, date: LocalDateTime)