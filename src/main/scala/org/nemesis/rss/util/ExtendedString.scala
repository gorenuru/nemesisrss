package org.nemesis.rss.util

import java.security.MessageDigest

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 10:14 AM
 */
class ExtendedString(source: String) {

  /**
   * Generates MD5 hash on current string
   * @return md5 hash
   */
  def hash: String = {
    val md5 = MessageDigest.getInstance("MD5")
    md5.reset()
    md5.update(source.getBytes("UTF-8"))
    md5.digest().map(0xFF &).map { "%02x".format(_) }.foldLeft(""){_ + _}
  }
}
