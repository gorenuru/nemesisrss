package org.nemesis.rss

import org.nemesis.rss.config.ActorConfiguration._
import org.nemesis.rss.logic._
import akka.pattern.ask
import scala.concurrent.Await
import org.nemesis.rss.logic.UserRegistered
import org.nemesis.rss.logic.Subscribe
import org.nemesis.rss.logic.Subscribed
import org.nemesis.rss.logic.RegisterUser
import org.nemesis.rss.dto.ParsedItem

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 7:59 AM
 */
object Test {
  def main(args: Array[String]) {
    val registration = Await.result(userManager ? RegisterUser("gorenuru", "demodemo"), timeout.duration).asInstanceOf[UserRegistered]
    val subscription = Await.result(subscriptionManager ? Subscribe(registration.id, "http://habrahabr.ru/rss/hubs/"), timeout.duration).asInstanceOf[Subscribed]

    Thread.sleep(20000)
    val items = Await.result(feedItemsManager ? GetUnreadItems(registration.id), timeout.duration).asInstanceOf[ItemsList]
    val toMark = items.items.drop(10).map(_.id).flatten

    feedItemsManager ? MarkAsRead(registration.id, toMark)
    val items2 = Await.result(feedItemsManager ? GetUnreadItems(1), timeout.duration).asInstanceOf[ItemsList]
    items2.items.foreach(println)
  }
}
