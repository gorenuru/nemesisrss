package org.nemesis.rss.domain

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 11:50 AM
 */
case class UserFeed(id: Option[Long], userId: Long, feedId: Long)