package org.nemesis.rss.domain

import org.joda.time.LocalDateTime

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 12:38 PM
 */
case class FeedItem(id: Option[Long], url: String, hashedUrl: String, title: String, content: String, date: LocalDateTime, feedId: Long)
