package org.nemesis.rss.domain

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 8:15 AM
 */
case class User(id: Option[Long], userName: String, password: String)