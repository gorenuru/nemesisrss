package org.nemesis.rss.domain

/**
 * User: aalbul
 * Date: 9/12/13
 * Time: 10:49 AM
 */
case class Feed(id: Option[Long], url: String)