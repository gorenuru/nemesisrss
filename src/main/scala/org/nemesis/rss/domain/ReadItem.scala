package org.nemesis.rss.domain

/**
 * User: aalbul
 * Date: 9/13/13
 * Time: 2:51 PM
 */
case class ReadItem(id: Option[Long], itemId: Long, userId: Long)
